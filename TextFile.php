<?php
//Run the tests in the test folder by running: ./vendor/bin/phpunit test/project_test.php from within this directory.

//Create a TextFile class in the TextFile.php that meets the following criteria:
class TextFile {

	//Create a single function that can be used to pass the testRead() test function in test/project_test.php
	function __call($funct, $var) {
		//*Should confirm that it is a get function
		if (strPos($funct, 'get') === 0) {
			
		//	$file = str_replace('get','', strtolower($funct));//* should only remove the first get
			$file = strtolower(substr($funct, 3));
			return file_get_contents(__DIR__ . '/fonts-jokes/' . $file . '.txt');
		} else {
			return false;
		}
	}
	
	// Create a second function called getGist() that does the following:
	function getGist() {
		//Pulls a Github gist with an id of 7417659 using their API.
		$url = "https://api.github.com/gists/7417659";

		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 4);
		$json = curl_exec($ch);
	    	curl_close($ch);
		$gist = json_decode($json);
		foreach($gist->files as $title=>$data) {
			//Saves the content of the gist locally to a file called riddle_me_this.txt in the same directory as TextFile.php

			$remote = $data->raw_url;
			$local = __DIR__ . '/' . $title .'.txt';
			//Copy remote file
			/*
			copy($remote, $local);
			//*/

			//Write contents of response 
			/*
			$file = $data->content;
			$fp = fopen($local, 'w');
			fwrite($fp, $file);
			fclose($fp);
			//*/

			//Write contents of remote file 
			//*
			$file = file_get_contents($remote);
			$fp = fopen($local, 'w');
			fwrite($fp, $file);
			fclose($fp);
			//*/

		}
		//Returns the path to the new riddle_me_this.txt file.
		$this->local = local;
		return $local; 
	}
	

}
$tf= new TextFile;
echo $tf->getDate();

?>
