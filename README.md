# UpSync Application Project

Follow the instructions below:

1. Fork this repository
2. Create a TextFile class in the TextFile.php that meets the following criteria:
    1. Create a **single** function that can be used to pass the `testRead()` test function in `test/project_test.php`
    2. Create a second function called `getGist()` that does the following:
        1. Pulls a Github gist with an id of `7417659` using their [API](http://developer.github.com/v3/).
        2. Saves the content of the gist locally to a file called `riddle_me_this.txt` in the same directory as `TextFile.php`
        3. Returns the path to the new `riddle_me_this.txt` file.
3. Run the tests in the `test` folder by running:
   ```
   ./vendor/bin/phpunit test/project_test.php
   ```
   from within this directory.
4. Make the tests pass.
5. Send us a link to your fork.
6. ???
7. Profit
